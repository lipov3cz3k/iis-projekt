﻿-- Adminer 3.3.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `dodavatel`;
CREATE TABLE `dodavatel` (
  `IDdodavatele` int(11) NOT NULL AUTO_INCREMENT,
  `Nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Adresa` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mesto` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `PSC` int(11) NOT NULL,
  `Email` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Telefon` varchar(15) COLLATE utf8_czech_ci NOT NULL,
  `Aktivni` int(1) NOT NULL,
  PRIMARY KEY (`IDdodavatele`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `dodavatel` (`IDdodavatele`, `Nazev`, `Adresa`, `Mesto`, `PSC`, `Email`, `Telefon`, `Aktivni`) VALUES
(1,	'OSCOM, s.r.o.',	'Školní 158',	'Humpolec',	39601,	'obchod@oscom.cz',	'+42060865803',	1),
(2,	'ELEKTRO UNIVERSAL',	'Pod dálnicí 5/959',	'Praha 4',	14000,	'bila.technika@elektrouniversal.cz',	'777814616',	1);

DROP TABLE IF EXISTS `faktura`;
CREATE TABLE `faktura` (
  `IDfaktury` int(11) NOT NULL AUTO_INCREMENT,
  `IDprodeje` int(11) NOT NULL,
  `Jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Prijmeni` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Adresa` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mesto` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `PSC` int(11) NOT NULL,
  PRIMARY KEY (`IDfaktury`),
  KEY `IDprodeje` (`IDprodeje`),
  CONSTRAINT `faktura_ibfk_1` FOREIGN KEY (`IDprodeje`) REFERENCES `prodej` (`IDprodeje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `kategorie`;
CREATE TABLE `kategorie` (
  `IDkategorie` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`IDkategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `kategorie` (`IDkategorie`, `nazev`) VALUES
(1,	'Ledničky'),
(2,	'Pračky'),
(3,	'Myčky'),
(4,	'Plazmové televize');

DROP TABLE IF EXISTS `objednavka`;
CREATE TABLE `objednavka` (
  `IDobjednavky` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date NOT NULL,
  `IDzamestnance` int(11) NOT NULL,
  `IDdodavatele` int(11) NOT NULL,
  `Stav` int(11) NOT NULL,
  PRIMARY KEY (`IDobjednavky`),
  KEY `IDzamestnance` (`IDzamestnance`),
  KEY `IDdodavatele` (`IDdodavatele`),
  CONSTRAINT `objednavka_ibfk_1` FOREIGN KEY (`IDzamestnance`) REFERENCES `zamestnanec` (`IDzamestnance`),
  CONSTRAINT `objednavka_ibfk_2` FOREIGN KEY (`IDdodavatele`) REFERENCES `dodavatel` (`IDdodavatele`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `objednavka` (`IDobjednavky`, `Datum`, `IDzamestnance`, `IDdodavatele`, `Stav`) VALUES
(1,	'2011-12-05',	2,	1,	3),
(2,	'2011-12-05',	2,	2,	3),
(3,	'2011-12-05',	2,	1,	2),
(4,	'2011-12-05',	2,	2,	1);

DROP TABLE IF EXISTS `prodej`;
CREATE TABLE `prodej` (
  `IDprodeje` int(11) NOT NULL AUTO_INCREMENT,
  `datum` datetime NOT NULL,
  `IDzamestnance` int(11) NOT NULL,
  `Hotovo` int(1) NOT NULL,
  PRIMARY KEY (`IDprodeje`),
  KEY `IDzamestnance` (`IDzamestnance`),
  CONSTRAINT `prodej_ibfk_1` FOREIGN KEY (`IDzamestnance`) REFERENCES `zamestnanec` (`IDzamestnance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `prodej` (`IDprodeje`, `datum`, `IDzamestnance`, `Hotovo`) VALUES
(1,	'2011-12-05 13:53:05',	2,	1),
(2,	'2011-12-05 13:53:44',	2,	0),
(3,	'2011-12-05 13:54:18',	2,	1),
(4,	'2011-12-05 14:08:44',	2,	0);

DROP TABLE IF EXISTS `zamestnanec`;
CREATE TABLE `zamestnanec` (
  `IDzamestnance` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `Heslo` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `Jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Prijmeni` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Adresa` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mesto` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `PSC` int(11) NOT NULL,
  `Opravneni` int(11) NOT NULL,
  `Aktivni` int(1) NOT NULL,
  PRIMARY KEY (`IDzamestnance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zamestnanec` (`IDzamestnance`, `Login`, `Heslo`, `Jmeno`, `Prijmeni`, `Adresa`, `Mesto`, `PSC`, `Opravneni`, `Aktivni`) VALUES
(1,	'Liposan',	'955db0b81ef1989b4a4dfeae8061a9a6',	'Tomáš',	'Lipovský',	'Palackého 348',	'Vamberk',	51753,	1,	1),
(2,	'Ondy',	'2c7691662b39ccb9a2413a749db0a27a',	'Tomáš',	'Ondrouch',	'Jesenická 348',	'Staré Město \'',	78789,	1,	0),
(3,	'alois',	'db88b0bec76802620750e83961ace431',	'Alois',	'Jirásek',	'',	'',	0,	1,	1),
(4,	'pepik',	'8827d52c113d97159c79be8c745f42b0',	'Josef',	'Skladnik',	'',	'',	0,	4,	1),
(5,	'sef',	'38f0b4a9f8d7ef4915f3b77e7a8d0ef9',	'Jindřich',	'Vedoucí',	'',	'',	0,	2,	1),
(6,	'babka',	'036ca8f266f5d71fdccd437ae035dbce',	'Jindřiška',	'Opíchalová',	'',	'',	0,	3,	1);

DROP TABLE IF EXISTS `zbozi`;
CREATE TABLE `zbozi` (
  `IDzbozi` int(11) NOT NULL AUTO_INCREMENT,
  `Nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mnozstvi_sklad` int(11) NOT NULL,
  `Mnozstvi_prodejna` int(11) NOT NULL,
  `Vyrobce` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `EAN` varchar(13) COLLATE utf8_czech_ci NOT NULL,
  `cena` decimal(10,2) NOT NULL,
  `IDkategorie` int(11) NOT NULL,
  `aktivni` int(1) NOT NULL,
  PRIMARY KEY (`IDzbozi`),
  KEY `IDkategorie` (`IDkategorie`),
  CONSTRAINT `zbozi_ibfk_1` FOREIGN KEY (`IDkategorie`) REFERENCES `kategorie` (`IDkategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zbozi` (`IDzbozi`, `Nazev`, `Mnozstvi_sklad`, `Mnozstvi_prodejna`, `Vyrobce`, `EAN`, `cena`, `IDkategorie`, `aktivni`) VALUES
(1,	'PS51D570',	4,	0,	'Samsung',	'15666',	18590.00,	4,	1),
(2,	'VIERA TX-P42G30E',	1,	1,	'Panasonic',	'879849',	15290.00,	4,	1),
(3,	'PS43D450',	3,	0,	'Samsung',	'1816',	8649.00,	4,	1),
(4,	'EWG 14550 W',	6,	1,	'Electrolux ',	'14656',	11059.00,	2,	1),
(5,	'3F 3612 IT',	3,	1,	'FAGOR',	'165147',	7294.00,	2,	1),
(6,	'AWO/D 062',	2,	2,	'Whirlpool',	'541984',	12470.00,	2,	1),
(7,	'FC 150.3',	2,	2,	'Amica',	'4876',	3890.00,	1,	1),
(8,	'RB 3125 W',	3,	2,	'Gorenje',	'165487',	3399.00,	1,	1),
(9,	'BRLF1760 W',	1,	2,	'Baumatic',	'541541',	7990.00,	1,	1),
(10,	'BDI652',	2,	2,	'Baumatic',	'1465',	6690.00,	3,	1),
(11,	'ADG 8900 FD',	7,	3,	'Whirlpool',	'1587',	16894.00,	3,	1),
(12,	'ESL 63010',	2,	3,	'Electrolux',	'1544154',	6429.00,	3,	1);

DROP TABLE IF EXISTS `zbozi_objednavka`;
CREATE TABLE `zbozi_objednavka` (
  `IDobjednavky` int(11) NOT NULL,
  `IDzbozi` int(11) NOT NULL,
  `Pocet` int(11) NOT NULL,
  `Cena` decimal(10,2) NOT NULL,
  KEY `IDobjednavky` (`IDobjednavky`),
  KEY `IDzbozi` (`IDzbozi`),
  CONSTRAINT `zbozi_objednavka_ibfk_1` FOREIGN KEY (`IDobjednavky`) REFERENCES `objednavka` (`IDobjednavky`),
  CONSTRAINT `zbozi_objednavka_ibfk_2` FOREIGN KEY (`IDzbozi`) REFERENCES `zbozi` (`IDzbozi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zbozi_objednavka` (`IDobjednavky`, `IDzbozi`, `Pocet`, `Cena`) VALUES
(1,	7,	4,	15560.00),
(1,	8,	2,	6798.00),
(1,	9,	3,	23970.00),
(1,	4,	3,	33177.00),
(1,	5,	4,	29176.00),
(1,	6,	2,	24940.00),
(1,	10,	2,	13380.00),
(1,	11,	8,	135152.00),
(1,	12,	5,	32145.00),
(1,	1,	2,	37180.00),
(1,	2,	4,	61160.00),
(1,	3,	3,	25947.00),
(2,	8,	3,	10197.00),
(2,	9,	1,	7990.00),
(2,	4,	5,	55295.00),
(2,	5,	1,	7294.00),
(2,	6,	2,	24940.00),
(2,	10,	2,	13380.00),
(2,	11,	4,	67576.00),
(2,	1,	3,	55770.00),
(2,	3,	1,	8649.00),
(3,	9,	2,	15980.00),
(4,	10,	3,	20070.00),
(4,	11,	2,	33788.00),
(4,	12,	2,	12858.00);

DROP TABLE IF EXISTS `zbozi_prodej`;
CREATE TABLE `zbozi_prodej` (
  `IDprodeje` int(11) NOT NULL,
  `IDzbozi` int(11) NOT NULL,
  `Pocet` int(11) NOT NULL,
  `Cena` decimal(10,2) NOT NULL,
  KEY `IDprodeje` (`IDprodeje`),
  KEY `IDzbozi` (`IDzbozi`),
  CONSTRAINT `zbozi_prodej_ibfk_1` FOREIGN KEY (`IDprodeje`) REFERENCES `prodej` (`IDprodeje`),
  CONSTRAINT `zbozi_prodej_ibfk_2` FOREIGN KEY (`IDzbozi`) REFERENCES `zbozi` (`IDzbozi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zbozi_prodej` (`IDprodeje`, `IDzbozi`, `Pocet`, `Cena`) VALUES
(1,	2,	2,	15290.00),
(1,	3,	1,	8649.00),
(2,	11,	2,	16894.00),
(2,	4,	1,	11059.00),
(2,	9,	1,	7990.00),
(3,	1,	1,	18590.00),
(3,	5,	1,	7294.00);

-- 2011-12-05 20:30:42