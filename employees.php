<?php


    require_once("./include/head.php"); 
    if (!isset($_GET['menu'])){$menu="list";}
    else{$menu = $_GET['menu'];} 
    $left_menu = array(">> Seznam zaměstnanců" => "employees.php?menu=list", ">> Editace" => "employees.php?menu=edit",  ">> Přidat zaměstnance" => "employees.php?menu=add");
    if ($_SESSION["isLogged"]){
        switch ($menu) {
            case "list":
                // seznam zamestnacu
                include("employees-list.php");  
            break;
            case "edit":
                // editace zamestnancu
                if ($user->isLogged && $user->perm == 1)
                    include("employees-edit.php");
                else
                    $data .= "<p>Nemáte dostatečná oprávnění na editaci zaměstnanců";
            break;
            case "add":
                // pridani zamestnance
                if ($user->isLogged and $user->perm == 1)
                    include("employees-create.php");
                else
                    $data .= "<p>Nemáte dostatečná oprávnění na editaci zaměstnanců";
            break;
            default:
                $data .= "WTF, něco si vyber.";
            break;
        }
    }
    else 
        $left_menu = array(">> Login" => "login.php");

    
    include("./include/layout.php");  
?>