<?php
    function printEmployees(){
        $query = MySQL_Query("
            SELECT IDzamestnance 
            FROM zamestnanec
            ORDER BY Prijmeni, Jmeno
        ") or die (mysql_error());
        while($result = mysql_fetch_assoc($query)){
                $employees[] = new User($result['IDzamestnance']);
        }

        $out = "<h3>Vyberte zaměstnance, kterého chcete editovat:</h3>
        <table border=1>
        <tr><th>Jméno</th><th>Login</th><th>Adresa</th><th>Oprávnění</th><th>E</th><th>D</th></tr>";

        foreach ($employees as $i => $value) {
            $perm = $value->recodePerm();
            $class = "";
            if ($value->activeAcc == 0)
                $class = " class=\"deleted\"";
            $out .= "
            <tr><td$class>".$value->name." ".$value->surname."</td><td$class>".$value->login."</td><td$class>".$value->adress."</td><td$class>".$perm."</td><td><a href=\"employees.php?menu=edit&amp;id=".$value->id."\"><img src=\"images/icon_edit.png\" alt=\"editovat\"></a></td><td><a href=\"employees.php?menu=edit&amp;a=delete&amp;id=".$value->id."\"><img src=\"images/icon_delete.png\" alt=\"odstranit\"></a></td></tr>";
        }
        $out .= "</table>"; 
        return $out; 
    } 
 
////////////////////////////////////////////////////////////////////////////////    
    
    
    if (!isset($_GET['id'])){
        $data .= printEmployees();
    }
    else {
        $id = $_GET['id'];
        $employee = new User($id);
        $action = "employees.php?menu=$menu&amp;id=$id&amp;a=edit";
        $disablePass = " disabled";
        if (isset($_GET["a"]) && $_GET["a"] == "delete"){
            if($employee->id != $user->id){
                if ($employee->delete($id))
                    $data .= "<p>Uživatel byl úspěšně deaktivován</p>";
                else
                    $data .= "<p>Uživatel byl úspěšně aktivován</p>";
            }
            else
                $data .= "<p>Nemůžete deaktivovat svůj vlastní účet</p>";
        }
        if (isset($_POST['zamID'])){
            $employee->name = mysql_escape_string($_POST['jmeno']);
            $employee->surname = mysql_escape_string($_POST['prijmeni']);
            $employee->street = mysql_escape_string($_POST['ulice']);
            $employee->city = mysql_escape_string($_POST['mesto']);
            $employee->postCode = mysql_escape_string($_POST['psc']);
            $employee->login = mysql_escape_string($_POST['login']);
            $employee->perm = mysql_escape_string($_POST['prava']);
            $employee->activeAcc = mysql_escape_string($_POST['aktivni']);
            
            if ($_GET["a"] == "edit"){
                if ($employee->id == $user->id and ($employee->perm != $user->perm or $employee->activeAcc != $user->activeAcc)){
                    $data .= "<p>Nelze změnit svá vlastní práva nebo deaktivovat svůj účet</p>";
                    $employee->perm = $user->perm;
                    $data .= "<h3>Editace zaměstnance:</h3>";
                    include("./include/employee-form.php");
                    
                }
                else {
                    if ($employee->actualize($_POST['zamID'], $employee->login, $employee->name, $employee->surname, $employee->street, $employee->city, $employee->postCode, $_POST['prava'], $_POST['aktivni']))
                    {
                        $data .= "<p>Informace o zaměstnanci byly aktualizovány</p>";
                        $data .= printEmployees();                
                    }
                    else {
                        $employee->login = mysql_escape_string($_POST['login']);
                        $data .= "<p>Informace o zaměstnanci nebyly aktulizovány - Vámi zadaný login již používá jiný zaměstnanec</p>";
                        $data .= "<h3>Editace zaměstnance:</h3>";
                        include("./include/employee-form.php");
                    }
                }
            }
            //else {

            //}

        }
        else if (!isset($_GET["a"])){
            $data .= "<h3>Editace zaměstnance:</h3>";
            include("./include/employee-form.php");
        
        }
    } 
 
?>