<?php
class Order {
    public $orderID;
    public $supplier;
    public $products;
    public $date;
    public $employee;
    public $state;
    
    
    
    public function __construct($id, $employee = NULL, $supplierID = NULL) {
        if($id == -1){
            //vytvori se nova objednavka    
            $query = MySQL_Query(
              "INSERT INTO objednavka (`datum`, `IDzamestnance`, `IDdodavatele`, `Stav`)
              VALUES ('".date("Y-m-d H:i:s")."', '$employee', '$supplierID', '1')"   
            ) or die (mysql_error());
            $this->orderID = mysql_insert_id();
        }else{
            // nacte se objednavka z db
            $this->orderID = $id;
            $query = MySQL_Query("SELECT * FROM zbozi_objednavka WHERE IDobjednavky = '$this->orderID'") or die (mysql_error());
            
            $this->products = array();
            while($result = mysql_fetch_assoc($query)){
                 array_push($this->products, array(new Product($result['IDzbozi']), $result['Pocet'], $result['Cena']));
            }
            
            $query = MySQL_Query("SELECT * FROM objednavka INNER JOIN dodavatel ON objednavka.IDdodavatele = dodavatel.IDdodavatele WHERE IDobjednavky = '$this->orderID'") or die (mysql_error());
            $result = mysql_fetch_assoc($query);
            $this->supplier = new Supplier($result['IDdodavatele']);
            
            $query = MySQL_Query("SELECT * FROM objednavka INNER JOIN zamestnanec ON zamestnanec.IDzamestnance = objednavka.IDzamestnance WHERE IDobjednavky = '$this->orderID'") or die (mysql_error());
            $result = mysql_fetch_assoc($query);
            $this->employee = new User($result['IDzamestnance']);
            
            $this->date = $result['Datum'];
            $this->state = $result['Stav'];
        }
    }
    
    public function insertProduct($productID, $count){
        $product = new Product($productID);
        $price = $count * $product->price;
        $query = MySQL_Query(
              "INSERT INTO zbozi_objednavka (`IDobjednavky`, `IDzbozi`, `Pocet`, `Cena`)
              VALUES ('$this->orderID', '$productID', '$count', '$price')"   
        ) or die (mysql_error());
    }
    
    public function printProducts(){
        $data = "
        <ul>";
        $sum = 0;
        foreach($this->products as $key => $value){
            $product = $value[0];
            $data .= "
            <li>".$value[1]." ks $product->name za cenu $value[2] Kč</li>";
            ;
            $sum += $value[2];
        }
        $data .= "
        </ul>
        Celková cena: <span class=\"order\">$sum Kč</span>";
        return $data;
        
    }
    
    public function delete(){
        if ($this->state < 3){
            $query = MySQL_Query(
                  "DELETE FROM zbozi_objednavka
                  WHERE IDobjednavky = '$this->orderID'"   
            ) or die (mysql_error());
            $query = MySQL_Query(
                  "DELETE FROM objednavka
                  WHERE IDobjednavky = '$this->orderID'"   
            ) or die (mysql_error());
            return true;
        }
        return false;
    }
    public function confirm(){
        if ($this->state < 3){
            $newState = $this->state + 1;
            $query = MySQL_Query(
                  "UPDATE objednavka
                  SET Stav = '$newState'
                  WHERE IDobjednavky = '$this->orderID'"   
            ) or die (mysql_error());
            if ($this->state == 1)
                return true;
            foreach($this->products as $key => $value){
                $value[0] = $value[0];
                $value[0]->stock += $value[1];
                $value[0]->insert();
            }  
            return true;
        }
        return false;
    }
    
    
    
}
?>