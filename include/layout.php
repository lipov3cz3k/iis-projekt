<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Style-Type" content="text/css">
  <meta http-equiv="Content-Language" content="cs">
  <meta http-equiv="Content-Script-Type" content="text/javascript">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Style-Type" content="text/css">

  <title>Obchod - administrace</title>  
  <link rel="stylesheet" type="text/css" href="css/admin.css">
  <link rel="stylesheet" type="text/css" href="css/admin_print.css" media="print">
  <link type="text/css" rel="stylesheet" href="css/prodej.css">
  <script type="text/javascript" src="./js/jquery-1.7.1.min.js"></script>
  <script src="./js/jafa.js" type="text/JavaScript" charset="utf-8"></script>
</head>
<body>

 
<div id="web_position">
    <div id="web_header"><a href=index.php><span>Obchod - administrace</span></a></div>
    <div id="web_menu">
	<ul>
            <?php
            echo $top_menu;
            ?>
	</ul>
    </div>
    <div id="web_page">
      <div id="web_left">
        <div class="left_box">
            <?php
                foreach($left_menu as $name => $url){
                    echo "<a href=\"$url\">$name</a>";  
                }
            ?>
        </div>
        <div class="left_box" id="box_login">
            <h3><span>Přihlášení</span></h3>
            <?php 
            if($user->isLogged){
                echo "Přihlášen $user->name<br>";
                echo '<a href="login.php?a=logout">Odhlásit</a><br>';
            }else{
                echo '<form action="login.php" method="POST">
                        <p class="login_form"><input type="text" name="login_usr" class="login_text"></p>
                        <p class="login_form"><input type="password" name="login_pass" class="login_text" size="15"><input type="submit" name="login_button" class="login_submit" value="GO"></p>
                      </form>';
            }
            
            ?>
        </div>
      </div>
      <div id="web_right">
        <?php 
        echo $data; 
        ?>
      </div>       
    </div> 
</div>
</body>    