<?php

require_once("./head.php"); 

if($user->perm > 3 || !$_SESSION["isLogged"]){
    header( 'Location: ../index.php' );
    echo "<script type=\"text/javascript\">
    window.location = \"../index.php\"
    </script>";
}

switch($_GET['type']){
    case "cat":
        $categoryID = $_GET['id'];
        $query = MySQL_Query("
          SELECT IDzbozi, Nazev, Vyrobce 
          FROM zbozi
          WHERE IDKategorie = '$categoryID'") or die (mysql_error());
        $json = '[';
        while($result = mysql_fetch_assoc($query)){
            if($json == '[')
                $json .= "{\"value\":".$result['IDzbozi'].", \"name\":\"".$result['Vyrobce']." - ".$result['Nazev']."\"}";
            else
                $json .= ",{\"value\":".$result['IDzbozi'].", \"name\":\"".$result['Vyrobce']." - ".$result['Nazev']."\"}";
           $products[$result['IDzbozi']] = $result['Nazev'];
        } 
        $json .= "]";
        echo $json;
       // echo json_encode($products);
    break;
    
    case "basket":
       if(isset($_GET['id'])){
           $saleID = $_GET['id'];

           $query = MySQL_Query("SELECT * FROM zbozi_prodej WHERE IDprodeje = '$saleID'") or die (mysql_error());
           
            $products = array();
            while($result = mysql_fetch_assoc($query)){
                 array_push($products, array(new Product($result['IDzbozi']), $result['Pocet'], $result['Cena']));
            }
            $json = '[';
            foreach($products as $i => $basket){
                if($json == '[')
                    $json .= "{\"name\":\"".$basket[0]->manufacturer." - ".$basket[0]->name."\", \"productID\":".$basket[0]->productID.", \"count\":".$basket[1].", \"price\":".$basket[2]."}";
                else
                    $json .= ",{\"name\":\"".$basket[0]->manufacturer." - ".$basket[0]->name."\", \"productID\":".$basket[0]->productID.", \"count\":".$basket[1].", \"price\":".$basket[2]."}";
            }
            $json .= ']';
            echo $json;
       }
    break;
    
    case "basketAdd":
        if(isset($_GET['saleID']) && isset($_GET['productID']) && isset($_GET['count'])){
            $saleID = $_GET['saleID'];
            $productID = $_GET['productID'];
            $count = $_GET['count'];
            
            $query = MySQL_Query("
              SELECT Mnozstvi_prodejna, cena
              FROM zbozi
              WHERE IDZbozi = '$productID'") or die (mysql_error());
            if($result = mysql_fetch_assoc($query)){
                $price = $result["cena"];
                if($result['Mnozstvi_prodejna'] >= $count){
                    // je to dobry, jeste mame
                    $query = MySQL_Query("
                      SELECT IDProdeje, IDzbozi, Pocet
                      FROM zbozi_prodej
                      WHERE IDProdeje = '$saleID' AND IDZbozi = '$productID'") or die (mysql_error());
                    if($result = mysql_fetch_assoc($query)){
                        if($result["Pocet"]+$count == 0){
                        // uz v kosiku nic nezbylo
                          $query = MySQL_Query(
                             "DELETE FROM zbozi_prodej
                             WHERE IDProdeje = '$saleID' AND IDZbozi = '$productID'"
                            ) or die (mysql_error());
                            echo "zbozi .$productID. bylo z saleID: ".$saleID." odstraneno";   
                        }else{
                        // update
                            $query = MySQL_Query(
                             "UPDATE zbozi_prodej
                             SET Pocet = Pocet+$count
                             WHERE IDProdeje = '$saleID' AND IDZbozi = '$productID'"
                            ) or die (mysql_error());
                            echo "pocet upraven saleID:".$saleID." productID: ".$productID." počet: ".$count;    
                        }
                    }else{            
                        $queryInsert = MySQL_Query(
                          "INSERT INTO zbozi_prodej (`IDProdeje`, `IDZbozi`, `Pocet`, `Cena`)
                          VALUES ('$saleID', '$productID', '$count','".$price."')"   
                        ) or die (mysql_error());
                        echo "data ulozena saleID:".$saleID." productID: ".$productID." počet: ".$count;    
                    }
                    // odeberu ze skladu(prodejny)
                    $query = MySQL_Query(
                     "UPDATE zbozi
                     SET Mnozstvi_prodejna = Mnozstvi_prodejna-$count
                     WHERE IDZbozi = '$productID'"
                    ) or die (mysql_error());
                }else
                    // nedostatek na sklade
                    echo "nedostatek zbozi na prodejne!";
            }
        }
    break;
    
    case "basketAddEan":
        if(isset($_GET['saleID']) && isset($_GET['productEAN']) && isset($_GET['count'])){
            $saleID = $_GET['saleID'];
            $productEAN = $_GET['productEAN'];
            $count = $_GET['count'];

            $query = MySQL_Query("
              SELECT IDZbozi, Mnozstvi_prodejna, cena
              FROM zbozi
              WHERE EAN = '$productEAN' AND Mnozstvi_prodejna > 0  AND Aktivni='1'
              LIMIT 1") or die (mysql_error());
            if($result = mysql_fetch_assoc($query)){
                $productID = $result["IDZbozi"];
                $price = $result["cena"];
                if($result['Mnozstvi_prodejna'] >= $count){
                    // je to dobry, jeste mame
                    $query = MySQL_Query("
                      SELECT IDProdeje, IDzbozi, Pocet
                      FROM zbozi_prodej
                      WHERE IDProdeje = '$saleID' AND IDZbozi = '$productID'") or die (mysql_error());
                    if($result = mysql_fetch_assoc($query)){
                        // update
                            $query = MySQL_Query(
                             "UPDATE zbozi_prodej
                             SET Pocet = Pocet+$count
                             WHERE IDProdeje = '$saleID' AND IDZbozi = '$productID'"
                            ) or die (mysql_error());
                            echo "pocet upraven saleID:".$saleID." productID: ".$productID." počet: ".$count;    
                    }else{            
                        $queryInsert = MySQL_Query(
                          "INSERT INTO zbozi_prodej (`IDProdeje`, `IDZbozi`, `Pocet`, `Cena`)
                          VALUES ('$saleID', '$productID', '$count','".$price."')"   
                        ) or die (mysql_error());
                        echo "data ulozena saleID:".$saleID." productID: ".$productID." počet: ".$count;    
                    }
                    // odeberu ze skladu(prodejny)
                    $query = MySQL_Query(
                     "UPDATE zbozi
                     SET Mnozstvi_prodejna = Mnozstvi_prodejna-$count
                     WHERE IDZbozi = '$productID'"
                    ) or die (mysql_error());
                }
            }else
                // nedostatek na sklade
                echo "nedostatek zbozi na prodejne!";
        }
    break;
    
    case "basketDelete":
        if(isset($_GET['saleID']) && isset($_GET['productID'])){
            $saleID = $_GET['saleID'];
            $productID = $_GET['productID'];
            
            $query = MySQL_Query("
              SELECT Pocet
              FROM zbozi_prodej
              WHERE IDProdeje = '$saleID' AND IDZbozi = '$productID'") or die (mysql_error());
            if($result = mysql_fetch_assoc($query)){
                $query = MySQL_Query(
                 "DELETE FROM zbozi_prodej
                 WHERE IDProdeje = '$saleID' AND IDZbozi = '$productID'"
                ) or die (mysql_error());
                echo "zbozi .$productID. bylo z saleID: ".$saleID." odstraneno";  
                
                // pripiseme na sklad
                $query = MySQL_Query(
                 "UPDATE zbozi
                 SET Mnozstvi_prodejna = Mnozstvi_prodejna+".$result["Pocet"]."
                 WHERE IDZbozi = '$productID'"
                ) or die (mysql_error());  
            }
        }
    break;
    case "confirmSale":
        if(isset($_GET['saleID'])){
            // update
            $saleID = $_GET['saleID'];
            $query = MySQL_Query(
             "UPDATE prodej
             SET Hotovo = 1
             WHERE IDProdeje = '$saleID'"
            ) or die (mysql_error());
            echo "saleID:".$saleID." potvrzen";    
        }
    break;
    default:
}
?>
