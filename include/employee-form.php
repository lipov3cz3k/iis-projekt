<?php
    $selected = array("", "", "", "", "");
    if(isset($employee->perm))
        $selected[$employee->perm] = "selected";
    $checked = "";
    if($employee->activeAcc)
        $checked = " checked";
    $data .= "
    <form class=\"edit_form\" name=\"form\" method=\"POST\" action=\"$action\" onSubmit=\"return checkEmployeeForm(form)\">
        <p class=\"duly\"><label>Příjmení:</label>
        <input name=\"prijmeni\" value=\"$employee->surname\" maxlength=\"40\" size=\"40\">
        <input name=\"zamID\" value=\"$employee->id\" type=\"hidden\"></p>
        <p class=\"duly\"><label>Jméno:</label>
        <input name=\"jmeno\" value=\"$employee->name\" maxlength=\"40\" size=\"40\"></p>
        <p class=\"duly\"><label>Login:</label>
        <input name=\"login\" value=\"$employee->login\" maxlength=\"40\" size=\"40\"></p>
        <p class=\"duly\"><label>Heslo:</label>
        <input name=\"heslo1\" value=\"$employee->password\" maxlength=\"40\" size=\"40\"  type=\"password\"$disablePass></p>
        <p class=\"duly\"><label>Potvrdit heslo:</label>
        <input name=\"heslo2\" value=\"$employee->password\" maxlength=\"40\" size=\"40\"  type=\"password\"$disablePass></p>
        <p><label>Ulice a ČP:</label>
        <input name=\"ulice\" value=\"$employee->street\" maxlength=\"40\" size=\"40\"></p>
        <p><label>Město:</label>
        <input name=\"mesto\" value=\"$employee->city\" maxlength=\"40\" size=\"40\"></p>
        <p><label>PSČ:</label>
        <input name=\"psc\" value=\"$employee->postCode\" maxlength=\"5\" size=\"5\"></p>
        <p class=\"duly\"><label>Typ zaměstnance:</label>
        <select name=\"prava\">
            <option value=\"3\"$selected[3]>Prodavač
            <option value=\"4\"$selected[4]>Skladník
            <option value=\"1\"$selected[1]>Vedoucí (s právem editovat zaměstnance)
            <option value=\"2\"$selected[2]>Vedoucí (bez práva editovat zaměstnance)
        </select></p>
        <p><label>Aktivní účet:</label>
        <input type=\"checkbox\" name=\"aktivni\" value=\"1\"$checked></p>
        <p><input name=\"reset\" value=\"Obnovit\" type=\"reset\">
        <input name=\"odeslat\" value=\"Odeslat\" type=\"submit\"></p>

    </form>
    ";
  



?>
