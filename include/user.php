<?php

/**
 * Třída pro uživatele
 *
 * @author Lipov3cz3k
 */
class User {
    public $id;
    public $login;
    public $password;
    public $name;
    public $surname;
    public $street;
    public $city;
    public $postCode;
    public $perm;
    public $isLogged;
    public $adress;
    public $activeAcc;
    
    public function __construct($id = -1, $isLogged = false, $logIn = true) {
        if ($id > -1){
          if($isLogged && $logIn)
              $this->isLogged = true;
          
          $query = MySQL_Query("
            SELECT *
            FROM zamestnanec 
            WHERE IDzamestnance = '$id'
            LIMIT 1
          ") or die (mysql_error());
  
          if ($result = mysql_fetch_assoc($query)){
            $this->id = $id;
            $this->name = $result['Jmeno'];
            $this->surname = $result['Prijmeni'];
            $this->login = $result['Login'];
            $this->password = $result['Heslo'];
            $this->street = $result['Adresa'];
            $this->city = $result['Mesto'];
            $this->postCode = $result['PSC'];
            $this->adress = $result['Adresa']." ".$result['Mesto']." ".$result['PSC'];
            $this->perm = $result['Opravneni'];
            $this->activeAcc = $result['Aktivni'];
          }
        }
        else {
            $this->isLogged = false; 
           
            $this->id = -1;
            $this->name = '';
            $this->surname = '';
            $this->login = '';
            $this->password = '';
            $this->street = '';
            $this->city = '';
            $this->postCode = '';
            $this->adress = '';
            $this->perm = 0;
            $this->activeAcc = 0;
        } 
    }
    



    public function login($userName, $password){
        $userName = mysql_escape_string($userName);
        $query = MySQL_Query("
          SELECT IDzamestnance, Login, Heslo, Jmeno, Opravneni, Aktivni
          FROM zamestnanec
          WHERE Login = '$userName'
          LIMIT 1
        ") or die (mysql_error());
        
          if ($result = mysql_fetch_assoc($query)){
              if($result['Heslo'] == md5($password) and $result['Aktivni'] == 1){
                  $this->isLogged = true;
                  $this->name = $result['Jmeno'];
                  $this->perm = $result['Opravneni'];
                  
                  $_SESSION["isLogged"] = true;
                  $_SESSION["userID"] = $result['IDzamestnance'];
                  $_SESSION["access_time"] = time();
                  return true;
              }
          }  
        
        $this->isLogged = false; 
        $_SESSION["isLogged"] = false;
        $_SESSION["userID"] = 0;
        return false;
    }
    
    public function logout(){
              
        $this->isLogged = false; 
       
        $this->id = 0;
        $this->name = '';
        $this->surname = '';
        $this->login = '';
        $this->password = '';
        $this->street = '';
        $this->city = '';
        $this->postCode = '';
        $this->adress = '';
        $this->perm = 0;
            
       $_SESSION["isLogged"] = false;
       $_SESSION["userID"] = 0; 
       return true;
    }

    public function checkLogins($id, $login){
        $query = MySQL_Query("
            SELECT * 
            FROM zamestnanec
            WHERE login = '$login'
            AND IDzamestnance <> '$id'
        ") or die (mysql_error());
        if($result = mysql_fetch_assoc($query)){
            return false;
        }
        return true;
    }
    
    public function actualize($id, $login, $name, $surname,  $street, $city, $postCode, $perm, $activeAcc){
        if ($this->checkLogins($id, $login)){
            $query = MySQL_Query("
                UPDATE zamestnanec
                SET Jmeno = '$name', Prijmeni = '$surname', Adresa = '$street', Mesto = '$city', PSC = '$postCode', Opravneni = '$perm', Login = '$login', Aktivni = '$activeAcc'
                WHERE IDzamestnance = '$id'"
            ) or die (mysql_error());
            return true;
        } 
        else {
            return false;
        }
    }

    
    public function insertEmpIntoDB($login, $password, $name, $surname,  $street, $city, $postCode, $perm, $activeAcc){
        if ($this->checkLogins(-1, $login)){
            $query = MySQL_Query("
              INSERT INTO zamestnanec
              VALUES ('', '$login', '$password', '$name', '$surname', '$street', '$city', '$postCode', '$perm', '$activeAcc')"
            ) or die (mysql_error());
            return true;
        }    
        else {
            return false;
        }
    }
    
    public function recodePerm(){
        switch($this->perm){
            case 1:
                return "Vedoucí s právem editovat zaměstnance";
            break;
            case 2:
                return "Vedoucí bez práva editovat zaměstnance";            
            break;
            case 3:
                return "Prodavač";
            break;
            case 4:
                return "Skladník";
            break;
            default:
            break;
        }
    }
    public function delete(){
        if ($this->activeAcc == 0)
            $this->activeAcc = 1;
        else
            $this->activeAcc = 0;
        $query = MySQL_Query("
            UPDATE zamestnanec
            SET Aktivni = $this->activeAcc
            WHERE IDzamestnance = '$this->id'"
        ) or die (mysql_error());
        if ($this->activeAcc){
            return false;
        } 
        return true;
    } 
    
    
}
?>
