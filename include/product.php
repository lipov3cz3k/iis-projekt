<?php

/**
 * Třída produktů
 *
 * @author Lipov3cz3k
 */
class Product {
    public $productID;
    public $categoryID;
    public $name;
    public $manufacturer;
    public $price;
    public $categoryName;
    public $ean;
    public $stock;
    public $shop;
    public $active;
    

    
    public function __construct($productID, $categoryID=NULL, $name=NULL, $manufacturer=NULL,$price=NULL, $ean=NULL, $stock=NULL, $shop=NULL, $active=NULL) {
        $this->productID = $productID;
        if($categoryID == NULL && $productID > 0){
        // nacteni produktu z db
            $query = MySQL_Query("SELECT * FROM zbozi WHERE IDzbozi = '$this->productID' LIMIT 1") or die (mysql_error());
            if ($result = mysql_fetch_assoc($query)){
                $this->categoryID = $result['IDkategorie'];
                $this->name = $result['Nazev'];
                $this->manufacturer = $result['Vyrobce'];
                $this->price = $result['cena'];
                $this->ean = $result['EAN'];
                $this->stock = $result['Mnozstvi_sklad'];
                $this->shop = $result['Mnozstvi_prodejna'];
                $this->active = $result['aktivni'];
            }else{
                $this->productID = -1;
            }
        }else{
        // nacteni z parametru
            $this->categoryID = $categoryID;
            $this->name = $name;
            $this->manufacturer = $manufacturer;
            $this->price = $price;
            $this->ean = $ean;
            $this->stock = $stock;
            $this->shop = $shop;
            $this->active = $active;
        }
        
        // doplneni nazvu kategorie
        $query = MySQL_Query("SELECT nazev FROM kategorie WHERE IDkategorie = '$this->categoryID' LIMIT 1") or die (mysql_error());
        if ($result = mysql_fetch_assoc($query)){
          $this->categoryName = $result['nazev'];
        } 
    }
    
    public function insert(){
        if($this->productID > 0){
            // aktualizace produktu
            $query = MySQL_Query(
                "UPDATE zbozi
                SET Nazev = '$this->name', Mnozstvi_sklad = '$this->stock', Mnozstvi_prodejna = '$this->shop', Vyrobce = '$this->manufacturer', EAN = '$this->ean', cena = '$this->price', IDkategorie = '$this->categoryID'
                WHERE IDZbozi = '$this->productID'"
            ) or die (mysql_error());
            return true;
        }else{
            // pridani produktu
            $query = MySQL_Query(
              "INSERT INTO zbozi (`Nazev`, `Mnozstvi_sklad`, `Mnozstvi_prodejna`, `Vyrobce`, `EAN`, `cena`, `IDkategorie`, `Aktivni` )
              VALUES ('$this->name', '$this->stock', '$this->shop','$this->manufacturer', '$this->ean', '$this->price', '$this->categoryID', '1')"   
            ) or die (mysql_error());
            return true;
        }
        return false;
    }
    
    public function addToShop($count){
        if ($count > $this->stock){
            return false;
        }
        $newShop = $this->shop + $count;
        $newStock = $this->stock - $count;
        $query = MySQL_Query(
            "UPDATE zbozi
            SET Mnozstvi_sklad = '$newStock', Mnozstvi_prodejna = '$newShop'
            WHERE IDZbozi = '$this->productID'"
        ) or die (mysql_error());
        return true;
    }
       
}

    function findProducts($maxShop = 0, $minStock = 100000, $name = "", $manufName = "", $categoryName = ""){
        
        if ($minStock < 0)
            $minStock = 0;
        $maxShop++;
        $query = MySQL_Query(
            "SELECT IDzbozi
            FROM zbozi
            INNER JOIN kategorie
            ON zbozi.IDkategorie = kategorie.IDkategorie
            WHERE zbozi.Nazev LIKE '%$name%'
            AND Mnozstvi_sklad > '$minStock'
            AND Mnozstvi_prodejna < '$maxShop'
            AND Vyrobce LIKE '%$manufName%'
            AND kategorie.nazev LIKE '%$categoryName%'
            ORDER BY Vyrobce, zbozi.Nazev"
            
        ) or die (mysql_error());
        while ($result = mysql_fetch_assoc($query))
            $products[] = new Product($result['IDzbozi']);
        return $products;
    }
    


?>
