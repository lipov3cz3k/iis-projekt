<?php

require_once("./head.php");

if (($_FILES["file"]["type"] == "text/xml")
 && ($_FILES["file"]["size"] < 20000)){
    
    if ($_FILES["file"]["error"] > 0){
       echo "Error: " . $_FILES["file"]["error"] . "<br />";
    }else{
        echo "Upload: " . $_FILES["file"]["name"] . "<br />";
        echo "Type: " . $_FILES["file"]["type"] . "<br />";
        echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
        echo "Stored in: " . $_FILES["file"]["tmp_name"]."<br>"; 
        
        // parsovani xml souboru
        $doc = new DOMDocument('1.0', 'UTF-8'); 
        $doc->load( $_FILES["file"]["tmp_name"] );
        $imported = array();
        
        $products = $doc->getElementsByTagName( "SHOPITEM" ); 
        foreach( $products as $product ) 
        { 
          $names = $product->getElementsByTagName( "PRODUCT" ); 
          $name = $names->item(0)->nodeValue; 

          $prices= $product->getElementsByTagName( "PRICE_VAT" ); 
          $price= $prices->item(0)->nodeValue; 

          $categorytexts = $product->getElementsByTagName( "CATEGORYTEXT" ); 
          $categorytext = $categorytexts->item(0)->nodeValue; 

          $eans = $product->getElementsByTagName( "EAN" ); 
          $ean = $eans->item(0)->nodeValue; 

          $manufacturers = $product->getElementsByTagName( "MANUFACTURER" ); 
          $manufacturer = $manufacturers->item(0)->nodeValue; 

          $params = $product->getElementsByTagName( "PARAM" ); 
          foreach( $params as $param ) 
          {
              $param_names = $param->getElementsByTagName( "PARAM_NAME" ); 
              $param_name = $param_names->item(0)->nodeValue;

              $param_vals = $param->getElementsByTagName( "VAL" ); 
              $param_val = $param_vals->item(0)->nodeValue;
              switch ($param_name) {
                  case 'productID':
                      $productID = $param_val;
                  break;
                  case 'categoryID':
                      $categoryID = $param_val;
                  break;
                  case 'stockCount':
                      $stockCount = $param_val;
                  break;
                  case 'shopCount':
                      $shopCount = $param_val;
                  break;
              }
          } 
          
          // import do databaze
         $product = new Product(-1, $categoryID, $name, $manufacturer, $price, $ean, $stockCount, $shopCount);       
            if($product->insert())
               echo $$name." - ".$manufacturer." pridan<br>";
          
          
         // array_push($imported, array($productID, $name, $manufacturer, $categoryID, $categorytext, $price, $ean, $stockCount, $shopCount));
            //echo "<b>$name - $price - $categorytext - $ean - $manufacturer</b> | PID $productID | CID $categoryID | stock $stockCount |shop $shopCount<br>"; 
        }
        
    }    
}else{
    echo "Špatný formát souboru";
}
?>
