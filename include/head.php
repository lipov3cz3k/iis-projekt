<?php

    ini_set("default_charset", "UTF-8");

    $data = "";
    $left_menu = "";
    $top_menu = "";
    require_once ("_mySQL.php");
    session_start();
    if(!isset($_SESSION["userID"]) ){
        $_SESSION["userID"] = 0;    
        $_SESSION["isLogged"] = false;
    }

    require_once("user.php");
    require_once("product.php");
    require_once("supplier.php");
    require_once("sale.php");
    require_once("order.php");

    $user = new User($_SESSION["userID"], $_SESSION["isLogged"]);
    if ($_SESSION["isLogged"] and $_SESSION["access_time"] < strtotime("-20 minutes")){
      $user->isLogged = false;   
      $_SESSION["isLogged"] = false;
      $_SESSION["userID"] = 0;
      $data = "<p>Byl jste automaticky odhlášen z důvodu neaktivity.<p>";
    }
    else if ($_SESSION["isLogged"])
        $_SESSION["access_time"] = time();
    
    
    
    switch($user->perm){
        case 1:
        case 2:
            $top_menu .= "<li><a href=\"index.php\">Úvod</a></li>
                <li><a href=\"sales.php\">Prodej</a></li>
                <li><a href=\"products.php\">Zboží</a></li>
                <li><a href=\"employees.php\">Zaměstnanci</a></li>
                <li><a href=\"suppliers.php\">Dodavatelé</a></li>
                <li><a href=\"orders.php\">Objednávky</a></li>"; 
        break;
        case 3:
            $top_menu .= "<li><a href=\"index.php\">Úvod</a></li>
                <li><a href=\"sales.php\">Prodej</a></li>"; 
        break;
        case 4:
            $top_menu .= "<li><a href=\"index.php\">Úvod</a></li>
                <li><a href=\"suppliers.php\">Dodavatelé</a></li>
                <li><a href=\"orders.php\">Objednávky</a></li>"; 
        break;
        default:
            $top_menu .= "<li><a href=\"index.php\">Úvod</a></li>
                <li><a href=\"login.php\">Přihlásit se</a></li>"; 
        break;
    }
    
    
?>
