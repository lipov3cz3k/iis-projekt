<?php
    
    
    require_once("./include/head.php"); 
    
    if (!isset($_GET['menu'])){$menu="default";}
    else{$menu = $_GET['menu'];}
    
    if(isset($_GET['a']))
        $a = $_GET['a'];
    else
        $a = "";
    
    //prodej left menu
    $left_menu = array(">> Pokladna" => "sales.php?menu=checkout",  ">> Vystavení účtenky" => "sales.php?menu=invoice");   

    switch ($menu) {
    case "checkout":
        $data .= "<h3>Pokladna</h3>";
        if(!isset ($_GET['id'])){
            // neni prirazeno ID prodeje
            
            if(isset($_GET['new']) && $_GET['new'] == "true"){
                $sale = new Sale(-1, $user->id);              
                header( 'Location: sales.php?menu=checkout&id='.$sale->saleID ) ;
                
                echo "<script type=\"text/javascript\">
                window.location = \"sales.php?menu=checkout&id=$sale->saleID\"
                </script>";
                
            }else{
                $data .= "<p>Chcete založit novou účtenku? <a href=\"sales.php?menu=checkout&new=true\"><b>ANO</b></a> <a href=\"sales.php?menu=invoice\">NE</a></p>";
            }
        }else{
            $data .= "<script type=\"text/javascript\" src=\"./js/checkout.js\"></script>";
            switch($a){
                case "send":
                    $sale = new Sale();
                    if($sale->insertFromBasket($_POST['resultHid'], "."))
                        $data .= "Data uložena, chcete <a href=\"#\">vytisknout účtenku</a>?";  

                //break;    
                default:                
                    $data .= " 
                    <table border=1>                    
                                 <tr>
                                <td>
                                 <p><label for=\"category\">Kategorie</label>
                                 <select size=\"10\" id=\"category\" name=\"category\">";       

                                $query = MySQL_Query("
                                  SELECT IDkategorie, Nazev
                                  FROM kategorie
                                    ") or die (mysql_error());
                                while($result = mysql_fetch_assoc($query)){
                                    $data .= "<option value=\"".$result['IDkategorie']."\">".$result['Nazev']."</option>";
                                } 
                 $data .= "</select></p>
                                </td>
                                 <td>
                                     <p><label for=\"ean\">EAN</label><input type=\"text\" id=\"ean\" name=\"ean\"></p>            
                                     <p><label for=\"products\">Zboží</label>
                                     <select size=\"10\" id=\"products\" name=\"products\">
                                     </select></p><input type=\"button\" value=\"&gt;&gt;\" name=\"addButton\" id=\"addButton\"> (TIP: použijte dvojklik na zboží)
                                 </td>
                                 <td>
                        <div id=\"basket\">";

                        $data .= "</div>
                                 </td>
                                 <td>
                                    <input name=\"odeslat\" value=\"Odeslat\" id=\"submit\"type=\"submit\">
                                 </td>
                                 </tr>

                    </table>
                    ";
            }
        }  
    break;
    case "invoice":
        if(isset ($_GET['id'])){
        // zobrazeni účtenky
            $sale = new Sale($_GET['id'], -1);
            if($sale->confirmed != 1){
                header( 'Location: sales.php?menu=checkout&id='.$sale->saleID ) ;
            
                echo "<script type=\"text/javascript\">
                window.location = \"sales.php?menu=checkout&id=$sale->saleID\"
                </script>";
            }
 
            $seller = new User($sale->userID, false, false);
            $data .= "<h2>Účtenka č.$sale->saleID</h2>
                    <p>Datum: $sale->date </p>
                    <p>Prodavač: $seller->name $seller->surname</p>
                    
                    <table border=\"1\">
                    <tr><th>ID</th><th>Název</th><th>Výrobce</th><th>Počet</th><th>Cena [Kč/ks]</th></tr>";
            $totalPrice = 0;
            foreach($sale->products as $i => $value){
                $data .= "<tr><td>".$value[0]->productID."</td><td>".$value[0]->name."</td><td>".$value[0]->manufacturer."</td><td>".$value[1]."</td><td>".$value[2]."</td></tr>";
                $totalPrice += $value[2]*$value[1];
            }        
            $data .= "<tr><td></td><td></td><td></td><th>Cena celkem:</th><th>$totalPrice Kč</th></tr></table>
            
            <p>Vystavil: $user->name $user->surname dne ".date ("d.m.Y")."</p>
            ";
            
        }else{
        // seznam účtenek
            $data .= "<h3>Seznam účtenek</h3>";
            
            $data .= "<table class=\"invoice\">
                        <tr><th>Číslo účtenky</th><th>Datum vystavení</th><th>Vystavil</th></tr>";                        
            
            $query = MySQL_Query("
                SELECT IDprodeje, datum, prodej.IDzamestnance, jmeno, prijmeni, Hotovo
                FROM prodej INNER JOIN zamestnanec ON prodej.IDzamestnance = zamestnanec.IDzamestnance
                ORDER BY datum DESC
                    ") or die (mysql_error());
            while($result = mysql_fetch_assoc($query)){
                if($result['Hotovo'] == 1)
                    $data .= "<tr><td class=\"ready\"><a href=\"sales.php?menu=invoice&amp;id=".$result['IDprodeje']."\">".$result['IDprodeje']."</a></td><td>".$result['datum']."</td><td>".$result['jmeno']." ".$result['prijmeni']."</td></tr>";   
                else
                    $data .= "<tr><td class=\"notready\"><a href=\"sales.php?menu=checkout&amp;id=".$result['IDprodeje']."\">".$result['IDprodeje']."</a></td><td>".$result['datum']."</td><td>".$result['jmeno']." ".$result['prijmeni']."</td><td><a href=\"sales.php?menu=checkout&amp;id=".$result['IDprodeje']."\">NÁKUP NENÍ DOKONČEN</a></td></tr>";                       
            }

            $data.=     "</table>";
            
        }
    break;
    default:
        $data .= "<p>Vlevo vyberte činnost.</p>";
    break;
    }
     
    if($_SESSION["isLogged"] && ($user->perm == 1 || $user->perm == 2 || $user->perm == 3))
        include("./include/layout.php");  
    else{
        header( 'Location: index.php' );
        echo "<script type=\"text/javascript\">
        window.location = \"index.php\"
        </script>";   
    }
    
    
?>
