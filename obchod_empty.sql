-- Adminer 3.3.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `dodavatel`;
CREATE TABLE `dodavatel` (
  `IDdodavatele` int(11) NOT NULL AUTO_INCREMENT,
  `Nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Adresa` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mesto` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `PSC` int(11) NOT NULL,
  `Email` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Telefon` varchar(15) COLLATE utf8_czech_ci NOT NULL,
  `Aktivni` int(1) NOT NULL,
  PRIMARY KEY (`IDdodavatele`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `faktura`;
CREATE TABLE `faktura` (
  `IDfaktury` int(11) NOT NULL AUTO_INCREMENT,
  `IDprodeje` int(11) NOT NULL,
  `Jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Prijmeni` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Adresa` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mesto` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `PSC` int(11) NOT NULL,
  PRIMARY KEY (`IDfaktury`),
  KEY `IDprodeje` (`IDprodeje`),
  CONSTRAINT `faktura_ibfk_1` FOREIGN KEY (`IDprodeje`) REFERENCES `prodej` (`IDprodeje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `kategorie`;
CREATE TABLE `kategorie` (
  `IDkategorie` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`IDkategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `kategorie` (`IDkategorie`, `nazev`) VALUES
(1,	'Ledničky'),
(2,	'Pračky'),
(3,	'Myčky');

DROP TABLE IF EXISTS `objednavka`;
CREATE TABLE `objednavka` (
  `IDobjednavky` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date NOT NULL,
  `IDzamestnance` int(11) NOT NULL,
  `IDdodavatele` int(11) NOT NULL,
  `Stav` int(11) NOT NULL,
  PRIMARY KEY (`IDobjednavky`),
  KEY `IDzamestnance` (`IDzamestnance`),
  KEY `IDdodavatele` (`IDdodavatele`),
  CONSTRAINT `objednavka_ibfk_1` FOREIGN KEY (`IDzamestnance`) REFERENCES `zamestnanec` (`IDzamestnance`),
  CONSTRAINT `objednavka_ibfk_2` FOREIGN KEY (`IDdodavatele`) REFERENCES `dodavatel` (`IDdodavatele`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `prodej`;
CREATE TABLE `prodej` (
  `IDprodeje` int(11) NOT NULL AUTO_INCREMENT,
  `datum` datetime NOT NULL,
  `IDzamestnance` int(11) NOT NULL,
  `Hotovo` int(1) NOT NULL,
  PRIMARY KEY (`IDprodeje`),
  KEY `IDzamestnance` (`IDzamestnance`),
  CONSTRAINT `prodej_ibfk_1` FOREIGN KEY (`IDzamestnance`) REFERENCES `zamestnanec` (`IDzamestnance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `zamestnanec`;
CREATE TABLE `zamestnanec` (
  `IDzamestnance` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `Heslo` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `Jmeno` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Prijmeni` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Adresa` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mesto` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `PSC` int(11) NOT NULL,
  `Opravneni` int(11) NOT NULL,
  `Aktivni` int(1) NOT NULL,
  PRIMARY KEY (`IDzamestnance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zamestnanec` (`IDzamestnance`, `Login`, `Heslo`, `Jmeno`, `Prijmeni`, `Adresa`, `Mesto`, `PSC`, `Opravneni`, `Aktivni`) VALUES
(1,	'Liposan',	'955db0b81ef1989b4a4dfeae8061a9a6',	'Tomáš',	'Lipovský',	'Palackého 348',	'Vamberk',	51753,	1,	1);

DROP TABLE IF EXISTS `zbozi`;
CREATE TABLE `zbozi` (
  `IDzbozi` int(11) NOT NULL AUTO_INCREMENT,
  `Nazev` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `Mnozstvi_sklad` int(11) NOT NULL,
  `Mnozstvi_prodejna` int(11) NOT NULL,
  `Vyrobce` varchar(40) COLLATE utf8_czech_ci NOT NULL,
  `EAN` varchar(13) COLLATE utf8_czech_ci NOT NULL,
  `cena` decimal(10,2) NOT NULL,
  `IDkategorie` int(11) NOT NULL,
  PRIMARY KEY (`IDzbozi`),
  KEY `IDkategorie` (`IDkategorie`),
  CONSTRAINT `zbozi_ibfk_1` FOREIGN KEY (`IDkategorie`) REFERENCES `kategorie` (`IDkategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `zbozi_objednavka`;
CREATE TABLE `zbozi_objednavka` (
  `IDobjednavky` int(11) NOT NULL,
  `IDzbozi` int(11) NOT NULL,
  `Pocet` int(11) NOT NULL,
  `Cena` decimal(10,2) NOT NULL,
  KEY `IDobjednavky` (`IDobjednavky`),
  KEY `IDzbozi` (`IDzbozi`),
  CONSTRAINT `zbozi_objednavka_ibfk_1` FOREIGN KEY (`IDobjednavky`) REFERENCES `objednavka` (`IDobjednavky`),
  CONSTRAINT `zbozi_objednavka_ibfk_2` FOREIGN KEY (`IDzbozi`) REFERENCES `zbozi` (`IDzbozi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `zbozi_prodej`;
CREATE TABLE `zbozi_prodej` (
  `IDprodeje` int(11) NOT NULL,
  `IDzbozi` int(11) NOT NULL,
  `Pocet` int(11) NOT NULL,
  `Cena` decimal(10,2) NOT NULL,
  KEY `IDprodeje` (`IDprodeje`),
  KEY `IDzbozi` (`IDzbozi`),
  CONSTRAINT `zbozi_prodej_ibfk_1` FOREIGN KEY (`IDprodeje`) REFERENCES `prodej` (`IDprodeje`),
  CONSTRAINT `zbozi_prodej_ibfk_2` FOREIGN KEY (`IDzbozi`) REFERENCES `zbozi` (`IDzbozi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- 2011-12-05 14:06:45