var basket = new Array();
var saleID = getUrlVars()["id"].replace("#", "");
$(document).ready(function(){
    
    showBasket();
     
    $("select#category").change(function(){  
        $.getJSON("./include/data.php",{type: 'cat', id: $(this).val()}, function(j){
          var options = '';
          for (var i = 0; i < j.length; i++) {
            options += '<option value="' + j[i].value + '">' + j[i].name + '</option>';
          }
          $("select#products").html(options);
        });
    });
    
    $("select#products").dblclick(function(){
        var productID = $(this).val();
        var url = "./include/data.php?type=basketAdd&saleID="+saleID+"&productID="+productID+"&count=1";
        $.get(url ,function(data){
            if(data == "nedostatek zbozi na prodejne!")
                alert(data);
           showBasket();
        });       
    });
    
    $("input#submit").click(function(){
        var url = "./include/data.php?type=confirmSale&saleID="+saleID;
        $.get(url ,function(data){
           window.location.replace("sales.php?menu=invoice&id="+saleID);
        });     
    });
    
    $("input#ean").keypress(function(e) {
        if(e.keyCode == 13){
            var ean = $("input#ean").val();
            if(!isNaN(ean) && ean != ""){
                var url = "./include/data.php?type=basketAddEan&saleID="+saleID+"&productEAN="+ean+"&count=1";
   
               $.get(url ,function(data){
                   alert(data);
                  showBasket();
               }); 
            }else{
                alert("Zadejte pořádné číslo!");
                $("input#ean").val("");
            }
                
        }
    });
    
    $("input#addButton").click(function(){
        
        
        var productID = $("select#products").val();
        if(productID > 0){
            var url = "./include/data.php?type=basketAdd&saleID="+saleID+"&productID="+productID+"&count=1";
            $.get(url ,function(data){
                if(data == "nedostatek zbozi na prodejne!")
                    alert(data);
               showBasket();
            });   
        }else{
            alert("Vyberte zboží");
        }
       
    });
});

function showBasket(){
    $.getJSON("./include/data.php",{type: 'basket', id: saleID}, function(j){
        $("div#basket").html('');
        for (var i = 0; i < j.length; i++) {                      
            $("div#basket").append(j[i].name + " (" + j[i].count + "ks) ... "+j[i].count * j[i].price+" Kč <a href=\"#\" class=\"control\"><img src=\"./images/icon_dec.png\" class=\"control\" id=\"dec_"+j[i].productID+"\"></a> <a href=\"#\" class=\"control\"><img src=\"./images/icon_delete.png\" class=\"control\" id=\"del_"+j[i].productID+"\"></a><br>"); 
        }
        
        $("img.control").click(function(){
           var productID = $(this).attr('id').substr(4);
           var url;
           if($(this).attr('id').substr(0, 3) == "dec"){
               // snizit o jeden
               url = "./include/data.php?type=basketAdd&saleID="+saleID+"&productID="+productID+"&count=-1";
           }else if($(this).attr('id').substr(0, 3) == "del"){
               // odebrat z kosiku
               url = "./include/data.php?type=basketDelete&saleID="+saleID+"&productID="+productID;
           }  
           $.get(url ,function(data){
              showBasket();
           });    
        });           
    });    
}

function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function moveOption(selectFrom, selectTo, deleteFrom){   
    var selectFromLen = selectFrom.length; 
    var selectedText = new Array();
    var selectedValues = new Array();
    var selectedCount = 0;
    var i;
    var tmpLen;
    
    // projde seznam a odebere vybrane polozky a prida je do pomocneho pole
    for(i = selectFromLen-1; i >= 0; i--){
        if(selectFrom.options[i].selected){
            selectedText[selectedCount] = selectFrom.options[i].text;
            selectedValues[selectedCount] = selectFrom.options[i].value;
            if(deleteFrom == true){
                tmpLen = selectFrom.length;
                if(tmpLen >0){
                    selectFrom.options[i] = null;
                }
            }
            selectedCount++;
        }
    }
    var newOpt;
    var selectToLen;
    for(i = selectedCount-1; i >= 0; i--){
        newOpt = new Option(selectedText[i], selectedValues[i]);
        selectToLen = selectTo.length;
        selectTo.options[selectToLen] = newOpt;
    } 
}

function makeLink(delim, selectStr, hiddenStr){
    var selectObj = document.getElementById(selectStr);
    var hiddenObj = document.getElementById(hiddenStr);
    hiddenObj.value = '';
    // projde cely seznam a z hodnot vytvori retezec
    // pri prvni polozce nepouzije oddelovac
    for(var i = 0; i < selectObj.options.length; i++){        
        hiddenObj.value = hiddenObj.value == '' ? selectObj.options[i].value : hiddenObj.value + delim + selectObj.options[i].value;
    }
}
