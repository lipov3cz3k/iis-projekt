function checkEmployeeForm(form){
    var regexp = /^[\D]+$/
    var str = form.prijmeni.value;
    var result = str.search(regexp);
    if (form.prijmeni.value == "" || result == -1){
        alert("Zadejte korektní příjmení zaměstnace!!!");
		form.prijmeni.focus();
		return false;
    }
    str = form.jmeno.value;
    result = str.search(regexp);
    if (form.jmeno.value == "" || result == -1){
        alert("Zadejte korektní jméno zaměstnace!!!");
		form.prijmeni.focus();
		return false;
    }
    var regexp = /^[a-zA-Z]+$/
    str = form.login.value;
    result = str.search(regexp);
    if (form.login.value == "" || result == -1){
        alert("Zadejte korektní login zaměstnace!!!");
		form.login.focus();
		return false;
    }
    
    if (form.heslo1.value != form.heslo2.value){
        alert("Byla zadána rozdílná hesla!!!");
		form.heslo1.focus();
		form.heslo1.value = "";
		form.heslo2.value = "";
		return false;
    }
}

function checkSupplierForm(form){

    str=form.nazev.value; 
    regexp=/^[\D]+$/;
    result=str.search(regexp);
    if (form.nazev.value == "" || result == -1){
        alert("Zadejte korektní název společnosti!!!");
		form.prijmeni.focus();
		return false;
    }
    if (form.ulice.value == ""){
        alert("Zadejte adresu společnosti!!!");
		form.ulice.focus();
		return false;
    }
    if (form.mesto.value == ""){
        alert("Zadejte město společnosti!!!");
		form.mesto.focus();
		return false;
    }
    str=form.psc.value; 
    regexp=/^[1-9]{1}[0-9]{4}$/;
    result=str.search(regexp);
    if (form.psc.value == "" || result == -1){
        alert("Zadejte korektní PSČ společnosti!!!");
		form.psc.focus();
		return false;
    }
    return true;
}



function checkAddToShop(form){
    if (form.plusShop.value < 0){
        alert("Nelze převést 0 a méně kusů zboží");
        return false;
    }
    if (form.oldStock.value < form.plusShop.value){
        alert("Nelze převést více zboží než je na skladě");
        return false;
    }
    return true;  
}


function checkProductForm(form){
    if (form.name.value == ""){
        alert("Zadejte název zboží!!!");
		form.name.focus();
		return false;
    }
    if (form.manufacturer.value == ""){
        alert("Zadejte výrobce!!!");
		form.manufacturer.focus();
		return false;
    }
    if (form.price.value == "" || isNaN(form.price.value)){
        alert("Zadejte cenu zboží!!!");
		form.price.focus();
		return false;
    }
    if (isNaN(form.stock.value)){
        alert("Zadejte počet kusů na skladě!!!");
		form.stock.focus();
		return false;
    }
    if (isNaN(form.shop.value)){
        alert("Zadejte počet kusů na prodejně!!!");
		form.shop.focus();
		return false;
    }

    return true;
}

function checkCategoryForm(form){
    if (form.name.value == ""){
        alert("Zadejte název kategorie!!!");
		form.name.focus();
		return false;
    }
    return true;
}