<?php

    function printSuppliers(){
        $query = MySQL_Query("
            SELECT IDdodavatele 
            FROM dodavatel
        ") or die (mysql_error());
        $suppliers = array();
        while($result = mysql_fetch_assoc($query)){
            $suppliers[] = new Supplier($result['IDdodavatele']);
        }

        $out = "<h3>Vyberte dodavatele, kterého chcete editovat:</h3>
        <table border=1>
            <tr><th>Název dodavatele</th><th>Adresa</th><th>Telefon</th><th>E-mail</th><th>E</th><th>D</th></tr>";
        
        foreach ($suppliers as $i => $value) {
            if ($value->active == 0)
                $class = " class=\"deleted\"";
            else
                $class = "";
            $out .= "
            <tr><td$class>".$value->name."</td><td$class>".$value->adress."</td><td$class>".$value->phone."</td><td$class>".$value->email."</td><td$class><a href=\"suppliers.php?menu=edit&amp;a=edit&amp;id=".$value->id."\"><img src=\"images/icon_edit.png\" alt=\"editovat\"></a></td><td><a href=\"suppliers.php?menu=edit&amp;a=delete&amp;id=".$value->id."\"><img src=\"images/icon_delete.png\"  alt=\"odstranit\"></a></td></tr>";



        }
        $out .= "</table>";
        return $out;
    }
    
    
////////////////////////////////////////////////////////////////////////////////
    
    
     if (isset($_GET['id'])) {  
        $id = $_GET['id'];
        $supplier = new Supplier($id);
    }
    if (isset($_POST['dodID'])){
        $supplier->actualize($_POST['dodID'], $_POST['nazev'], $_POST['ulice'], $_POST['mesto'], $_POST['psc'], $_POST['email'], $_POST['telefon']);
        $data .= "<p>Informace o dodovateli byly aktualizovány</p>";
        $data .= printSuppliers();
    }
    if (isset($_GET['id']) and isset($_GET['a']) and $_GET['a'] == "delete"){
        
        if($supplier->delete()){
            $data .= "<p>Dodavatel byl úspěšně odstraněn</p>
            ";            
        }
        else 
            $data .= "<p>Dodavatel byl úspěšně aktivován</p>
            ";
        $data .= printSuppliers();
    }
    else if (!isset($_GET['id']) || $_GET['a'] == "delete"){
        $data .= printSuppliers();
    }
    else if (isset($_GET['id']) and isset($_GET['a']) and $_GET['a'] == "edit"){
        $id = $_GET['id'];
        $supplier = new Supplier($id);       
        $action = "suppliers.php?menu=edit&amp;id=$id";
        include("./include/supplier-form.php");
    }  
?>