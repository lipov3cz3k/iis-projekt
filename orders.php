<?php 

    require_once("./include/head.php"); 
    if (!isset($_GET['menu'])){$menu="list";}
    else{$menu = $_GET['menu'];}
    
    $left_menu = array(">> Nepřijaté objednávky" => "orders.php?menu=list", ">> Přijaté objednávky" => "orders.php?menu=agreed",  ">> Přidání objednávky" => "orders.php?menu=add",  ">> Doplnit zboží na prodejně" => "orders.php?menu=complete");
    
    if (($user->perm == 4 or $user->perm < 3) and $user->isLogged == true){
        if (!isset($_GET['a'])){$action="list";}
        else{$action = $_GET['a'];}
       
        switch ($menu) {
        case "list":
            switch ($action) {
            case "delete":
                $order = new Order($_GET['id']);
                if ($order->delete())
                    $data .= "
                    <p>Objednávka byla úspěšně stornována</p>";
                else
                    $data .= "
                    <p>Tuto objednávku není možné stornovat - Pravděpodobně se pokoušíte stornovat již přijatou objednávku.</p>";
            case "confirm":
                if ($action == "confirm"){
                    $order = new Order($_GET['id']);
                    if ($order->confirm()){
                        if ($order->state == 1)
                            $data .= "
                            <p>Objednávka byla úspěšně označena za odeslanou.</p>";
                        else
                            $data .= "
                            <p>Objednávka byla úspěšně označena za přijatou. Byly aktualizovány počty zboží na skladě.</p>";
                    }
                    else
                        $data .= "
                        <p>Tuto objednávku není možné potvrdit - Pravděpodobně se pokoušíte potvrdit již přijatou objednávku</p>";
                }
            default:
                $orders = array();
                $data .= "
                <h3>Neodeslané objednávky:</h3>";
                $query = MySQL_Query("
                    SELECT * 
                    FROM objednavka
                    WHERE Stav = '1'
                    ORDER BY Datum DESC
                ") or die (mysql_error());
                while($result = mysql_fetch_assoc($query)){
                    $orders[] = new Order($result['IDobjednavky']);
                }
                if (count($orders) == 0){
                    $data .= "
                    <p>V současné době neexistují neodeslané objednávky</p>";
                }
                foreach($orders as $i => $value){
                    $supplier = $value->supplier;
                    $employee = $value->employee;
                    $data .= "
                    <p class=\"order\">
                    Objednávka č. $value->orderID pro společnost $supplier->name vytvořená ".date("j. F Y", strtotime($value->date))." zaměstnancem $employee->name $employee->surname</p>
                    ";
                    $data .= "
                    ".$value->printProducts()."
                    <p><a href=\"orders.php?menu=list&amp;a=delete&amp;id=$value->orderID\">Stornovat objednávku</a></p>
                    <p><a href=\"orders.php?menu=list&amp;a=confirm&amp;id=$value->orderID\">Potvrdit objednávku</a></p>
                    ";
                }
                
                $orders = array();
                
                $data .= "
                <h3>Potvrzené nepřijaté objednávky:</h3>";
                $query = MySQL_Query("
                    SELECT * 
                    FROM objednavka
                    WHERE Stav = '2'
                    ORDER BY Datum DESC
                ") or die (mysql_error());
                while($result = mysql_fetch_assoc($query)){
                    $orders[] = new Order($result['IDobjednavky']);
                }
                if (count($orders) == 0){
                    $data .= "
                    <p>V současné době neexistují potvrzené nepřijaté objednávky</p>";
                }
                foreach($orders as $i => $value){
                    $supplier = $value->supplier;
                    $employee = $value->employee;
                    $data .= "
                    <p class=\"order\">
                Objednávka č. $value->orderID pro společnost $supplier->name vytvořená ".date("j. F Y", strtotime($value->date))." zaměstnancem $employee->name $employee->surname</p>
                    ";
                    $data .= "
                    ".$value->printProducts()."
                    <p><a href=\"orders.php?menu=list&amp;a=delete&amp;id=$value->orderID\">Stornovat objednávku</a></p>
                    <p><a href=\"orders.php?menu=list&amp;a=confirm&amp;id=$value->orderID\">Potvrdit příjem zboží</a></p>";
                }
            }
        
        break;
        
        case "add":
            switch($action){
                
            case "insert":
                $supplierID = $_POST['IDdodavatele'];
                $query = MySQL_Query("
                    SELECT IDzbozi 
                    FROM zbozi
                ") or die (mysql_error());        
                while($result = mysql_fetch_assoc($query)){
                    $products[] = new Product($result['IDzbozi']);
                }
                foreach($products as $key => $value){
                    if ($_POST["zbozi$value->productID"] > 0){
                        $productCount[$value->productID] = mysql_escape_string($_POST["zbozi$value->productID"]);
                    }
                }
                //$order = new Order();
                if (count($productCount) < 1){
                    $data .= "
                    <p>Nebylo zadáno žádné zboží k objednání</p>";

                }
                else {
                    $order = new Order(-1, $_SESSION['userID'], $supplierID);
                    $data .= "
                    <p>Byla vytvořena objednávka s následujícími položkami:</p>
                    <ul>";
                    foreach($productCount as $key => $value){
                        $order->insertProduct($key, $value);
                        $product = new Product($key);
                        $data .= "
                        <li>$product->name ($value ks objednáno)</li>";
                    }
                    $data .= "
                    </ul>";
                }
                $products = array();
            default:
                $query = MySQL_Query("
                    SELECT IDdodavatele 
                    FROM dodavatel
                    WHERE aktivni = '1'
                ") or die (mysql_error());
                $suppliers = array();
                while($result = mysql_fetch_assoc($query)){
                    $suppliers[] = new Supplier($result['IDdodavatele']);
                }
                $data .= "<h3>Vytvoření objednávky</h3>";
                $data .= "
                <form name=\"objednavka\" class=\"new_order_form\" method=\"POST\" action=\"orders.php?menu=add&amp;a=insert\">
                
                <p><label for=\"Dodavatel\">Dodavatel:</label>
                <select name=\"IDdodavatele\"></p>";
                foreach($suppliers as $i => $value){
                    $data .= "
                    <option value=\"$value->id\">".$value->name."</option>";
                }
                $data .= "
                </select>";
                
                
                $query = MySQL_Query("
                    SELECT * 
                    FROM kategorie
                    INNER JOIN zbozi
                    ON kategorie.IDkategorie = zbozi.IDkategorie
                    ORDER BY kategorie.nazev, zbozi.Nazev
                ") or die (mysql_error());        
                $products = array();
                while($result = mysql_fetch_assoc($query)){
                    $products[] = new Product($result['IDzbozi'], $result['IDkategorie'], $result['Nazev'], $result['Vyrobce'], $result['cena']);
                }
                $category = "";
                foreach($products as $i => $value){
                if ($value->categoryName != $category){
                    $category = $value->categoryName;
                    $data .= "
                    <h4>$category:</h4>";
                }
                    $data .= "
                    <p><label for=\"zbozi$value->productID\">$value->name ($value->price Kč/ks):</label>
                <input name=\"zbozi$value->productID\" value=\"0\" maxlength=\"3\" size=\"3\"></p>";
                }
                
                $data .= "
                <p><input name=\"reset\" value=\"Obnovit\" type=\"reset\">
                <input name=\"odeslat\" value=\"Odeslat\" type=\"submit\"></p>
                </form>
                ";
            break;
            }
        break;
        case "complete":
            $action = "list";
            if (isset($_GET['a']))
                $action = $_GET['a'];
            switch ($action){
            case "complete":
                $product = new Product($_GET['id']);
                $data .= "
                <h3>Přidání zboží na prodejnu ($product->name):</h3>
                <form name=\"addToShop\" class=\"edit_form\" method=\"POST\" action=\"orders.php?menu=complete&amp;a=add\" onSubmit=\"return checkAddToShop(addToShop)\">
                <p><label for=\"oldStock\">Kusů na skladě:</label>
                <input name=\"oldStock\" value=\"$product->stock\" type=\"hidden\">$product->stock</p>
                <p><label for=\"oldShop\">Kusů na prodejně:</label>
                <input name=\"oldShop\" value=\"$product->shop\" type=\"hidden\">$product->shop</p>
                <p><label for=\"plusShop\">Převést na prodejnu (ks):</label>
                <input name=\"plusShop\" value=\"0\" maxlength=\"40\" size=\"40\">
                <input name=\"productID\" value=\"$product->productID\" type=\"hidden\"></p>
                <p><input name=\"odeslat\" value=\"Odeslat\" type=\"submit\"></p>
                </form>

                ";
            break;
            case "add":
                $product = new Product($_POST['productID']);
                if ($product->addToShop($_POST['plusShop']))
                    $data .= "
                    <p>".$_POST['plusShop']." ks $product->name bylo úspěšně převedeno ze skladu na prodejnu.</p>";
                else 
                    $data .= "
                    <p>".$_POST['plusShop']." kusů $product->name nebylo převedeno ze skladu na prodejnu. Pravděpodobně chcete převést více zboží, než je na skladě.</p>";
            case "list":
                $maxShop = 10000;
                $minStock = 1;
                $name = "";
                $katName = "";
                $manufName = "";
                if (isset($_POST['maxShop']))
                    $maxShop = mysql_escape_string($_POST['maxShop']);
                if (isset($_POST['minStock']))
                    $minStock = mysql_escape_string($_POST['minStock']);  
                if (isset($_POST['name']))
                    $name = mysql_escape_string($_POST['name']);
                if (isset($_POST['manufName']))
                    $manufName = mysql_escape_string($_POST['manufName']);
                if (isset($_POST['katName']))
                    $katName = mysql_escape_string($_POST['katName']);
                $data .= "
                <h3>Vyberte zboží, které chcete doplnit.</h3>";
                $data .= "<p>Zadejte parametry vyhledávání zboží:</p>
                <form name=\"completeProducts\" class=\"complete_form\" method=\"POST\" action=\"orders.php?menu=complete&amp;a=list\">
                <p><label for=\"maxShop\">Maximálně ks na prodejně:</label>
                <input name=\"maxShop\" value=\"$maxShop\" maxlength=\"5\" size=\"20\"></p>
                <p><label for=\"minStock\">Minimálně ks na skladě:</label>
                <input name=\"minStock\" value=\"$minStock\" maxlength=\"5\" size=\"20\"></p>
                <p><label for=\"name\">Část názvu zboží:</label>
                <input name=\"name\" value=\"$name\" maxlength=\"40\" size=\"20\"></p>
                <p><label for=\"manufName\">Část názvu výrobce:</label>
                <input name=\"manufName\" value=\"$manufName\" maxlength=\"40\" size=\"20\"></p>
                <p><label for=\"katName\">Část názvu kategorie:</label>
                <input name=\"katName\" value=\"$katName\" maxlength=\"40\" size=\"20\"></p>
                <p><input name=\"reset\" value=\"Obnovit\" type=\"reset\">
                <input name=\"odeslat\" value=\"Odeslat\" type=\"submit\"></p>
                </form>
                <h5>Seznam zboží na skladě vyhovující vašim požadavkům:</h5>";
                $products = findProducts($maxShop, $minStock - 1, $name, $manufName, $katName);
                if (count($products) == 0){
                    $data .= "
                    <p>Vámi zadaným požadavkům nevyhovuje žádné zboží</p>";
                }
                else {
                    $data .= "
                    <ul>";
                    foreach($products as $key => $value){
                        $data .= "
                        <li>$value->manufacturer $value->name (sklad: $value->stock ks, prodejna $value->shop ks) <a href=\"orders.php?menu=complete&amp;a=complete&amp;id=$value->productID\">Doplnit zboží</a></li>";
                    }
                    $data .= "
                    </ul>";
                }
            break;
            default:
                $data .= "wtf něco si vyber!";
            break;
            }
            
        break;
        case "agreed":
            $orders = array();
            $data .= "
            <h3>Potvrzené přijaté objednávky:</h3>";
            $query = MySQL_Query("
                SELECT * 
                FROM objednavka
                WHERE Stav = '3'
                ORDER BY Datum DESC
            ") or die (mysql_error());
            while($result = mysql_fetch_assoc($query)){
                $orders[] = new Order($result['IDobjednavky']);
            }
            foreach($orders as $i => $value){
                $supplier = $value->supplier;
                $employee = $value->employee;
                $data .= "
                <p class=\"order\">
                Objednávka č. $value->orderID pro společnost $supplier->name vytvořená ".date("j. F Y", strtotime($value->date))." zaměstnancem $employee->name $employee->surname</p>
                ";
                $data .= "
                ".$value->printProducts();
            }
        break;
        default:
            $data .= "wtf něco si vyber!";
        break;
        
        
        }

    }
    
    else {
        $data .= "<p>Nemáte oprávnění k přidávání a editaci objednávek</p>";
    }
    
    include("./include/layout.php"); 
?>
