<?php
require_once("./include/head.php");
$query = MySQL_Query("
      SELECT IDzbozi, Nazev, Mnozstvi_sklad, Mnozstvi_prodejna, Vyrobce, EAN, cena, IDkategorie 
      FROM zbozi
        ") or die (mysql_error());
while($result = mysql_fetch_assoc($query)){
    $products[] = new Product($result['IDzbozi'], $result['IDkategorie'], $result['Nazev'], $result['Vyrobce'], $result['cena'], $result['EAN'], $result['Mnozstvi_sklad'], $result['Mnozstvi_prodejna']);
} 

$doc = new DOMDocument('1.0', 'UTF-8'); 
$doc->formatOutput = true; 

$r = $doc->createElement( "SHOP" ); 
$doc->appendChild( $r ); 

foreach ($products as $i => $value) {
$b = $doc->createElement( "SHOPITEM" ); 

$name = $doc->createElement( "PRODUCT" ); 
$name->appendChild( 
    $doc->createTextNode( $value->name ) 
); 
$b->appendChild( $name ); 

$price = $doc->createElement( "PRICE_VAT" ); 
$price->appendChild( 
    $doc->createTextNode( $value->price ) 
); 
$b->appendChild( $price );

$manufacturer = $doc->createElement( "MANUFACTURER" ); 
$manufacturer->appendChild( 
    $doc->createTextNode( $value->manufacturer ) 
); 
$b->appendChild( $manufacturer );

$categoryName = $doc->createElement( "CATEGORYTEXT" ); 
$categoryName->appendChild( 
    $doc->createTextNode( $value->categoryName ) 
); 
$b->appendChild( $categoryName );

$ean = $doc->createElement( "EAN" ); 
$ean->appendChild( 
    $doc->createTextNode( $value->ean ) 
); 
$b->appendChild( $ean );


$param = $doc->createElement( "PARAM" );
   $param_name = $doc->createElement( "PARAM_NAME" );
   $param_name->appendChild( 
        $doc->createTextNode( "productID" ) 
   ); 
   $param->appendChild( $param_name );   

   $param_name = $doc->createElement( "VAL" );
   $param_name->appendChild( 
        $doc->createTextNode( $value->productID ) 
   ); 
   $param->appendChild( $param_name );  
$b->appendChild( $param );

$param = $doc->createElement( "PARAM" );
   $param_name = $doc->createElement( "PARAM_NAME" );
   $param_name->appendChild( 
        $doc->createTextNode( "categoryID" ) 
   ); 
   $param->appendChild( $param_name );   

   $param_name = $doc->createElement( "VAL" );
   $param_name->appendChild( 
        $doc->createTextNode( $value->categoryID ) 
   ); 
   $param->appendChild( $param_name );  
$b->appendChild( $param );

$param = $doc->createElement( "PARAM" );
   $param_name = $doc->createElement( "PARAM_NAME" );
   $param_name->appendChild( 
        $doc->createTextNode( "stockCount" ) 
   ); 
   $param->appendChild( $param_name );   

   $param_name = $doc->createElement( "VAL" );
   $param_name->appendChild( 
        $doc->createTextNode( $value->stock ) 
   ); 
   $param->appendChild( $param_name );  
$b->appendChild( $param );

$param = $doc->createElement( "PARAM" );
   $param_name = $doc->createElement( "PARAM_NAME" );
   $param_name->appendChild( 
        $doc->createTextNode( "shopCount" ) 
   ); 
   $param->appendChild( $param_name );   

   $param_name = $doc->createElement( "VAL" );
   $param_name->appendChild( 
        $doc->createTextNode( $value->shop ) 
   ); 
   $param->appendChild( $param_name );  
$b->appendChild( $param );

$r->appendChild( $b ); 
}

//TODO: oprávnění

header("Content-disposition: attachment; filename=products.xml");
header("Content-type: text/xml; charset=UTF-8");
echo $doc->saveXML(); 
//$doc->save("products.xml")
?>      